;;; elisp-mapchars.el ---  map characters to a string  -*- lexical-binding: t -*-

;; Copyright (C) 2021, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20211110
;; Updated: 20211110
;; Version: 0.0
;; Keywords: string

;;; Commentary:

;; Some functions for mapping over strings, working with characters.

;; The function and macro names here should perhaps be rethought. PH20240516.

;;; Change Log:

;;; Code:


(defun mapchars (fun sequence)
  "Return string concatenation of applying (FUN char) for char in SEQUENCE.
See also: `mapchars*'."
  (concat
   (mapcar fun sequence))
  )


(defmacro mapchars* (seq &rest body)
  "Return string concatenation of evaluating BODY for chars in SEQ bound to c.
Note that, unlike `mapchars', the sequence argument comes first.
Example:
   (mapchars*  \"tagcaAtd2\"  (or (if (= c ?a) ?A) c))
   ==>  \"tAgcAAtd2\"
"
  (declare (indent 1))
  `(progn
     (cl-check-type ,seq sequence)
     (concat
      (mapcar
       (lambda (c)
         ,@body)
       ,seq))))



(provide 'mapchars)

;;; elisp-mapchars.el ends here
