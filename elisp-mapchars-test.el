;;; elisp-mapchars-test.el --- try out mapchars  -*- lexical-binding: t -*-

;; Copyright (C) 2024, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20240516
;; Updated: 20240516
;; Version: 0.0
;; Keywords:

;;; Commentary:

;;; Change Log:

;;; Code:


(require 'mapchars)


(ert-deftest mapchars ()
  (should
   (equal "dog"
          (mapchars #'cdr '((0 . ?d) (1 . ?o) (2 . ?g)))
          )
   ))


(ert-deftest mapchars* ()
  (should
   (equal "tAgcAAtd2"
          (mapchars* "tagcaAtd2"
            (or (if (= c ?a) ?A) c)
            ))
   ))


;;; elisp-mapchars-test.el ends here
